package me.taur.taurcmd.commands;

import me.taur.taurcmd.TaurCmd;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class PlayerCommands implements CommandExecutor {

	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		if (l.equalsIgnoreCase("heal")) {
			if (args.length > 2) {
				if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.YELLOW + "Usage: /" + l.toString() + " [player]");
				} else {
					s.sendMessage(ChatColor.YELLOW + "Usage: /heal [player]");
				}
			} else {
				if (args.length > 1) {
					if (s.hasPermission("taurcmd.heal.other") || s.isOp()) {
						Player target = Bukkit.getServer().getPlayer(args[0]);
						if (target != null) {
							target.setHealth(20);
							target.setFoodLevel(20);
							((Player) s).sendMessage(ChatColor.GREEN + target.getName() + "'s health is now full!");
							target.sendMessage(ChatColor.GREEN + ((Player) s).getName() + " restored your health!");
						} else {
							((Player) s).sendMessage(TaurCmd.plugin.PlayerOffline);
						}
					} else {
						((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					if (s.hasPermission("taurcmd.heal") || s.isOp()) {
						((Player) s).setHealth(20);
						((Player) s).setFoodLevel(20);
						((Player) s).sendMessage(ChatColor.GREEN + "Your health bar is now full!");
					} else {
						((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
					}
				}
			}

		if (l.equalsIgnoreCase("god")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.YELLOW + "Usage: /" + l.toString() + " [on/off]");
				} else {
					s.sendMessage(ChatColor.YELLOW + "Usage: /god [on/off]");
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.god") || s.isOp()) {
						if (args[0].equalsIgnoreCase("on")) {
							TaurCmd.plugin.getConfig().set("database.players." + ((Player) s).getName() + ".god", true);
							((Player) s).sendMessage(ChatColor.GREEN + "God mode enabled.");
							TaurCmd.plugin.saveConfig();
						}

						if (args[0].equalsIgnoreCase("off")) {
							TaurCmd.plugin.getConfig().set("database.players." + ((Player) s).getName() + ".god", false);
							((Player) s).sendMessage(ChatColor.DARK_GREEN + "God mode disabled.");
							TaurCmd.plugin.saveConfig();
						}
					} else {
						((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					s.sendMessage(ChatColor.RED + "You can't turn God mode on in-console.");
				}
			}
		}

		if (l.equalsIgnoreCase("slap"))
		{
			if (args.length < 2) {
				if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.YELLOW + "Usage: /" + l.toString() + " [player] [hardness]");
				} else {
					return false;
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.slap")) {
						if(Integer.parseInt(args[1]) > 10000) {
							Player p = (Player) s;
							p.sendMessage(ChatColor.RED + "You can't use that high a number!");
							return true;
						}
						Player target = Bukkit.getServer().getPlayer(args[0]);
						if (target != null) {
							double i = Integer.parseInt(args[1]) * 0.4;
							target.setVelocity(new Vector(i, i, 0));
							target.sendMessage(ChatColor.GREEN
									+ ((Player) s).getName() + " slapped you!");
							((Player) s).sendMessage(ChatColor.GREEN
									+ "You slapped " + target.getName());
						} else {
							((Player) s)
							.sendMessage(TaurCmd.plugin.PlayerOffline);
						}
					} else {
						((Player) s)
						.sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					if(Integer.parseInt(args[1]) > 10000) {
						Player p = (Player) s;
						p.sendMessage(ChatColor.RED + "You can't use that high a number!");
						return true;
					}
					Player target = Bukkit.getServer().getPlayer(args[0]);
					if (target != null)
					{
						double i = Integer.parseInt(args[1]) * 0.4;
						target.setVelocity(new Vector(i, i, 0));
						s.sendMessage(ChatColor.GREEN + "You slapped " + target.getName());
					} else {
						s.sendMessage(TaurCmd.plugin.PlayerOffline);
					}
				}
			}
		}

		if (l.equalsIgnoreCase("clear")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.clear")) {
						((Player) s).getInventory().clear();
						((Player) s).sendMessage(ChatColor.GREEN + "Inventory cleared!");
					} else {
						((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					s.sendMessage(ChatColor.YELLOW + "Usage: /clear [player]");
				}
			} else {
				if (s instanceof Player) {
					Player target = Bukkit.getServer().getPlayer(args[0]);
					if (s.hasPermission("taurcmd.clear.other")) {
						if (target != null) {
							target.getInventory().clear();
							target.sendMessage(ChatColor.GREEN + ((Player) s).getName() + " cleared your inventory!");
							((Player) s).sendMessage(ChatColor.GREEN + "Successfully cleared " + target.getName() + "'s inventory!");
						} else {
							((Player) s).sendMessage(TaurCmd.plugin.PlayerOffline);
						}
					} else {
						((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					Player target = Bukkit.getServer().getPlayer(args[0]);
					if (target != null) {
						target.getInventory().clear();
						s.sendMessage(ChatColor.GREEN + "You cleared " + target.getName() + "'s inventory!");
					} else {
						s.sendMessage(TaurCmd.plugin.PlayerOffline);
					}
				}
			}
		}

		if (l.equalsIgnoreCase("i")) {
			if (!(s instanceof Player)) {
			s.sendMessage(ChatColor.RED + "You can't spawn items for Console!");
			return false;
			}
			
			if (args.length < 1) {
				if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.YELLOW + "Usage: /" + l.toString() + " [id] [amount]");
				} else {
					s.sendMessage(ChatColor.YELLOW + "Usage: /i [id] [amount]");
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.item")) {
						String[] itemAndData = args[0].split(":");
			            int item = 0;
			            try {
			                item = Integer.parseInt(itemAndData[0]);
			            } catch (NumberFormatException ex) {
			                // Item was not an int, do nothing
			            }
			            Material itemMat = item == 0 ? Material.matchMaterial(itemAndData[0]) : Material.getMaterial(item);
			            int damage = 0;
			            if (itemMat == null) {
			                s.sendMessage(ChatColor.RED + "Invalid item '" + itemAndData[0] + "'!");
			                return true;
			            }
			            if (itemAndData.length == 2) {
			                try {
			                    damage = Integer.parseInt(itemAndData[1]);
			                } catch (NumberFormatException ex) {
			                    // Data was not an int, complain here
			                    s.sendMessage(ChatColor.RED + "Data Value must be an integer!");
			                    return true;
			                }
			            }
			            ItemStack itemStack = new ItemStack(itemMat, 1, (short) damage);
			            if (args.length == 1) {
			                ((Player) s).getInventory().addItem(itemStack);
			            } else {
			                int amount = 1;
			                try {
			                    amount = Integer.parseInt(args[1]);
			                } catch (NumberFormatException ex) {
			                    s.sendMessage(ChatColor.RED + "Invalid item amount!");
			                    return true;
			                }
			                itemStack.setAmount(amount);
			                ((Player) s).getInventory().addItem(itemStack);
			            }
			            s.sendMessage(ChatColor.GREEN + "You've been given " + itemStack.getAmount() + " of " + itemMat.toString() + " (" + itemMat.getId() + ")!");
								}
							}
						}
					} else {
						((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
				}

		if (l.equalsIgnoreCase("kill")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.YELLOW + "Usage: /" + l.toString() + " [player]");
				} else {
					s.sendMessage(ChatColor.YELLOW + "Usage: /kill [player]");
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.kill")) {
						Player target = Bukkit.getServer().getPlayer(args[0]);
						if (target != null) {
							target.setHealth(0);
							((Player) s).sendMessage(ChatColor.GREEN
									+ "You killed " + target.getName());
							target.sendMessage(ChatColor.DARK_GREEN + ((Player) s).getName() + " killed you!");
						} else {
							((Player) s).sendMessage(TaurCmd.plugin.PlayerOffline);
						}
					} else {
						((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					Player target = Bukkit.getServer().getPlayer(args[0]);
					if (target != null) {
						target.setHealth(0);
						s.sendMessage(ChatColor.GREEN + "You killed " + target.getName());
					} else {
						s.sendMessage(TaurCmd.plugin.PlayerOffline);
					}
				}
			}
		}

		if (l.equalsIgnoreCase("more")) {
			if (s instanceof Player) {
				if (s.hasPermission("taurcmd.more")) {
					((Player) s).getInventory().addItem(new ItemStack(((Player) s).getItemInHand().getType(), 63));
					((Player) s).sendMessage(ChatColor.GREEN + "You've received a stack of the item you're holding!");
				} else {
					((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
				}
			} else {
				s.sendMessage(ChatColor.RED + "You can't do that in-console!");
				}
			}
		}
		return false;
	}
}