package me.taur.taurcmd.commands;

import me.taur.taurcmd.TaurCmd;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeleportCommands implements CommandExecutor {

	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		if (l.equalsIgnoreCase("tp")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					s.sendMessage(ChatColor.YELLOW + "/" + l.toString() + " [player]");
				} else {
					s.sendMessage(ChatColor.YELLOW + "/tp [player]");
				}
			} else {
				if (s instanceof Player) {
					if (args.length == 2) {
						if (!s.hasPermission("taurcmd.tpother")) {
							Player target = Bukkit.getServer().getPlayer(args[0]);
							Player target2 = Bukkit.getServer().getPlayer(args[1]);
							if (target != null && target2 != null) {
								target.teleport(target2);
								s.sendMessage(ChatColor.GREEN + "Teleported" + target.getName() + " to " + target2.getName());
							} else {
								s.sendMessage(TaurCmd.plugin.PlayerOffline);
							}
						} else {
							((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
						}
					} else {
						if (s.hasPermission("taurcmd.tp")) {
							Player target = Bukkit.getServer().getPlayer(args[0]);
							if (target != null) {
								((Player) s).teleport(target.getLocation());
								((Player) s).sendMessage(ChatColor.GREEN
										+ "Teleported to " + target.getName());
							} else {
								((Player) s).sendMessage(TaurCmd.plugin.PlayerOffline);
							}
						} else {
						((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
						}
					}
				}
			}
		}

		if (l.equalsIgnoreCase("tphere")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.YELLOW + "/" + l.toString() + " [player]");
				} else {
					((Player) s).sendMessage(ChatColor.YELLOW + "/tphere [player]");
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.tphere")) {
						Player target = Bukkit.getServer().getPlayer(args[0]);
						if (target != null) {
							target.teleport(((Player) s).getLocation());
							((Player) s).sendMessage(ChatColor.GREEN + "Teleported " + target.getName() + " to you!");
							target.sendMessage(((Player) s).getName() + " teleported you!");
						} else {
							((Player) s).sendMessage(TaurCmd.plugin.PlayerOffline);
						}
					} else {
						((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					s.sendMessage(ChatColor.RED + "You can't teleport a player to you, you're console silly!");
				}
			}
		}
		

		if (l.equalsIgnoreCase("put")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.YELLOW + "/" + l.toString() + " [player]");
				} else {
					((Player) s).sendMessage(ChatColor.YELLOW + "/put [player]");
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.put")) {
						Player target = Bukkit.getServer().getPlayer(args[0]);
						if (target != null) {
							Location place = ((Player) s)
									.getTargetBlock(null, 0)
									.getRelative(BlockFace.UP, 2).getLocation();
							target.teleport(place);
							((Player) s).sendMessage(ChatColor.GREEN + "Teleported " + target.getName() + " to where you're looking at!");
							target.sendMessage(ChatColor.GREEN + ((Player) s).getName() + " teleported you!");
						} else {
							((Player) s).sendMessage(TaurCmd.plugin.PlayerOffline);
						}
					} else {
						((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					s.sendMessage(ChatColor.RED + "You are not in-game so you can't put a player where you're looking at!");
				}
			}
		}

		if (l.equalsIgnoreCase("setspawn")) {
			if (s instanceof Player) {
				if (s.hasPermission("taurcmd.setspawn")) {
					((Player) s).getWorld().setSpawnLocation(
							(int) ((Player) s).getLocation().getX(),
							(int) ((Player) s).getLocation().getY(),
							(int) ((Player) s).getLocation().getZ());
					((Player) s).sendMessage(ChatColor.GREEN + " World spawn set!");
				} else {
					((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
				}
			} else {
				s.sendMessage(ChatColor.RED + "You must be in-game to set the spawn!");
			}
		}

		if (l.equalsIgnoreCase("spawn")) {
			if (s instanceof Player) {
				if (s.hasPermission("taurcmd.spawn")) {
					((Player) s).teleport(((Player) s).getWorld()
							.getSpawnLocation());
					((Player) s).sendMessage(ChatColor.GREEN
							+ "Teleported to Spawn in '"
							+ ((Player) s).getWorld().getName().toLowerCase()
							+ "'");
				} else {
					((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
				}
			} else {
				if (args.length < 1) {
					s.sendMessage(ChatColor.YELLOW + "/spawn [player]");
				} else {
					Player target = Bukkit.getServer().getPlayer(args[0]);
					if (target != null) {
						target.teleport(target.getWorld().getSpawnLocation());
						s.sendMessage(ChatColor.GREEN + "Teleported " + target.getName() + " to spawn!");
					} else {
						s.sendMessage(TaurCmd.plugin.PlayerOffline);
					}
				}
			}
		}

		if (l.equalsIgnoreCase("tpall")) {
			if (s instanceof Player) {
				if (s.hasPermission("taurcmd.tpall")) {
					for (Player p : Bukkit.getServer().getOnlinePlayers()) {
						if(!p.getDisplayName().equals(((Player) s).getDisplayName())) {
							p.teleport(((Player) s).getLocation());
							p.sendMessage(ChatColor.GREEN + ((Player) s).getName() + " teleported you!");
							((Player) s).sendMessage(ChatColor.GREEN + "Teleported everyone to you!");
						}
					}
				} else {
					((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
				}
			} else {
				s.sendMessage(ChatColor.RED + "You must be in-game to teleport everyone to you!");
			}
		}

		if (l.equalsIgnoreCase("tpworld")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.YELLOW + "/" + l.toString() + " [world]");
				} else {
					s.sendMessage(ChatColor.YELLOW + "/tpworld [player] [world]");
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.tpworld")) {
						if (Bukkit.getServer().getWorld(args[0]) != null) {
							((Player) s).teleport(Bukkit.getServer()
									.getWorld(args[0]).getSpawnLocation());
							((Player) s).sendMessage(ChatColor.GREEN + "Teleported to world '" + Bukkit.getServer().getWorld(args[0]).getName() + "'");
						} else {
							((Player) s).sendMessage(ChatColor.RED + "World '" + args[0] + "' does not exist.");
						}
					} else {
						((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					if (Bukkit.getServer().getWorld(args[1]) != null) {
						Player target = Bukkit.getServer().getPlayer(args[0]);
						if (target != null) {
							target.teleport(Bukkit.getServer()
									.getWorld(args[1]).getSpawnLocation());
							s.sendMessage(ChatColor.GREEN + "Teleported " + target.getName() + " to '" + args[1] + "'");
						} else {
							s.sendMessage(TaurCmd.plugin.PlayerOffline);
						}
					} else {
						s.sendMessage(ChatColor.RED + "This world does not exist.");
					}
				}
			}
		}
		return false;
	}
}