package me.taur.taurcmd.commands;

import me.taur.taurcmd.TaurCmd;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class JoinLeaveCommands implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		if (l.equalsIgnoreCase("join")) {
			if (args.length < 0) {
				if(s.hasPermission("taurcmd.join.other")) {
					Bukkit.getServer().broadcastMessage(ChatColor.YELLOW + ((Player) s).getName() + " joined the game.");
				} else {
					((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
				}
			} else {
				if (s.hasPermission("taurcmd.leave.other")) {
					Bukkit.getServer().broadcastMessage(ChatColor.YELLOW + args[0] + " joined the game.");
				} else {
					((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
				}
			}
		}

		if (l.equalsIgnoreCase("leave")) {
			if (args.length < 0) {
				if(s.hasPermission("taurcmd.leave.other")) {
					Bukkit.getServer().broadcastMessage(ChatColor.YELLOW + ((Player) s).getName() + " left the game.");
				} else {
					((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
				}
			} else {
				if (s.hasPermission("taurcmd.leave")) {
					Bukkit.getServer().broadcastMessage(ChatColor.YELLOW + args[0] + " left the game.");
				} else {
					((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
				}
			}
		}
		return false;
	}
}