package me.taur.taurcmd.commands;

import me.taur.taurcmd.TaurCmd;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SkyCommands implements CommandExecutor {

	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		if (l.equalsIgnoreCase("time")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.YELLOW + "Usage: /" + l.toString() + " [day/night]");
				} else {
					s.sendMessage(ChatColor.YELLOW + "Usage: /time [day/night] [world]");
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.time")) {
						if (args[0].equalsIgnoreCase("day")) {
							((Player) s).sendMessage(ChatColor.GREEN + "You have set it to daytime!");
							((Player) s).getWorld().setTime(0);
						}

						if (args[0].equalsIgnoreCase("night")) {
							((Player) s).sendMessage(ChatColor.GREEN + "You have set it to nighttime!");
							((Player) s).getWorld().setTime(13047);
						}
					} else {
						((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					if (args[0].equalsIgnoreCase("day")) {
						if (Bukkit.getServer().getWorld(args[1]) != null) {
							Bukkit.getServer().getWorld(args[1]).setTime(0);
							s.sendMessage(ChatColor.GREEN + "Set " + args[1] + "'s time to day!");
						} else {
							s.sendMessage(ChatColor.RED + "This world does not exist. Did you type it correctly?");
						}
					}

					if (args[0].equalsIgnoreCase("night")) {
						if (Bukkit.getServer().getWorld(args[1]) != null) {
							Bukkit.getServer().getWorld(args[1])
									.setTime(13047);
							s.sendMessage(ChatColor.GREEN + "Set " + args[1] + "'s time to night!");
						} else {
							s.sendMessage(ChatColor.RED + "This world does not exist. Did you type it correctly?");
						}
					}
				}
			}
		}
		
		if (l.equalsIgnoreCase("weather")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.YELLOW + "Usage: /" + l.toString() + " [sun/rain] [world]");
				} else {
					s.sendMessage(ChatColor.YELLOW + "Usage: /weather [sun/rain] [world]");
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.weather")) {
						if (args[0].equalsIgnoreCase("sun")) {
							((Player) s).sendMessage(ChatColor.GREEN + "You made it shine!");
							((Player) s).getWorld().setStorm(false);
						}

						if (args[0].equalsIgnoreCase("rain")) {
							((Player) s).sendMessage(ChatColor.GREEN + "You made it rain!");
							((Player) s).getWorld().setStorm(true);
							((Player) s).getWorld().setThundering(true);
						}
					} else {
						((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					if (args[0].equalsIgnoreCase("sun")) {
						if (Bukkit.getServer().getWorld(args[1]) != null) {
							Bukkit.getServer().getWorld(args[1]).setStorm(false);
							s.sendMessage(ChatColor.GREEN + "You made " + args[1] + " shine!");
						} else {
							s.sendMessage(ChatColor.RED + "This world does not exist. Did you type it correctly?");
						}
					}

					if (args[0].equalsIgnoreCase("rain")) {
						if (Bukkit.getServer().getWorld(args[1]) != null) {
							Bukkit.getServer().getWorld(args[1]).setStorm(true);
							Bukkit.getServer().getWorld(args[1]).setThundering(true);
							s.sendMessage(ChatColor.DARK_GREEN + "You made " + args[0] + " rain!");
						} else {
							s.sendMessage(ChatColor.RED + "This world does not exist. Did you type it correctly?");
						}
					}
				}
			}
		}
		return false;
	}

}
