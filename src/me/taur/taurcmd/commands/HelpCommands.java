package me.taur.taurcmd.commands;

import me.taur.taurcmd.TaurCmd;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HelpCommands implements CommandExecutor {

	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {

		if (l.equalsIgnoreCase("help")) {
			if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.YELLOW + "Visit http://embarker.me/forums for help!");
					} else {
						((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
					}
			} else {
				s.sendMessage(ChatColor.RED
						+ "You can only view the commands in-game!");
			}
		return false;
	}

}
