package me.taur.taurcmd.commands;

import me.taur.taurcmd.TaurCmd;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GameModeCommands implements CommandExecutor {

	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		if (l.equalsIgnoreCase("creative")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.creative")) {
						((Player) s).setGameMode(GameMode.CREATIVE);
						((Player) s).sendMessage(ChatColor.GREEN
								+ "You gamemode is now creative!");
					} else {
						((Player) s)
								.sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					s.sendMessage(ChatColor.YELLOW + "Usage: /creative [player]");
				}
			} else {

				Player target = Bukkit.getServer().getPlayer(args[0]);

				if (s instanceof Player) {
					if (!s.hasPermission("taurcmd.creative.other")) {
						if (target != null) {
							target.setGameMode(GameMode.CREATIVE);
							((Player) s).sendMessage(ChatColor.GREEN
									+ target.getName()
									+ "'s gamemode is now creative!");
						} else {
							((Player) s)
									.sendMessage(TaurCmd.plugin.PlayerOffline);
						}
					} else {
						((Player) s)
								.sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					if (target != null) {
						target.setGameMode(GameMode.CREATIVE);
						s.sendMessage(ChatColor.GREEN + target.getName()
								+ "'s gamemode is now creative!");
					} else {
						s.sendMessage(TaurCmd.plugin.PlayerOffline);
					}
				}
			}
		}

		if (l.equalsIgnoreCase("survival")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.survival")) {
						((Player) s).setGameMode(GameMode.SURVIVAL);
						((Player) s).sendMessage(ChatColor.GREEN
								+ "Your gamemode is now survival!");
					} else {
						((Player) s)
								.sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					s.sendMessage(ChatColor.YELLOW + "Usage: /survival [player]");
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.survival.other")) {
						Player target = Bukkit.getServer().getPlayer(args[0]);
						if (target != null) {
							target.setGameMode(GameMode.SURVIVAL);
							((Player) s).sendMessage(ChatColor.GREEN
									+ target.getName()
									+ "'s gamemode is now survival!");
						} else {
							((Player) s)
									.sendMessage(TaurCmd.plugin.PlayerOffline);
						}
					} else {
						((Player) s)
								.sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					Player target = Bukkit.getServer().getPlayer(args[0]);
					if (target != null) {
						target.setGameMode(GameMode.SURVIVAL);
						s.sendMessage(ChatColor.GREEN + target.getName()
								+ "'s gamemode is now survival!");
					} else {
						s.sendMessage(TaurCmd.plugin.PlayerOffline);
					}
				}
			}
		}
		return false;
	}

}
