package me.taur.taurcmd.commands;

import me.taur.taurcmd.TaurCmd;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SayCommand implements CommandExecutor {

	String console = TaurCmd.plugin.getConfig().getString(
			"settings.consolename");

	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		if (l.equalsIgnoreCase("say")) {
			if (args.length < 1) {
				s.sendMessage(ChatColor.YELLOW + "Usage: /say [message]");
			} else {
				// Say stuff.
				StringBuilder x = new StringBuilder();
				int i;
				for (i = 0; i < args.length; i++) {
					x.append(args[i] + " ");
				}

				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.say")) {
						Bukkit.getServer().broadcastMessage(
								"<" + ChatColor.RED + console + ChatColor.WHITE
										+ "> " + ChatColor.WHITE
										+ x.toString().trim());
					} else {
						s.sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					Bukkit.getServer().broadcastMessage(
							"<" + ChatColor.RED + console + ChatColor.WHITE
									+ "> " + ChatColor.WHITE
									+ x.toString().trim());
				}
			}
		}
		
		if (l.equalsIgnoreCase("w")) {
			if (args.length < 2) {
				if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.YELLOW + "/" + l.toString()
							+ " [player] [message]");
				} else {
					s.sendMessage(ChatColor.YELLOW + "Usage: /w [player] [message]");
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.whisper")) {
						Player target = Bukkit.getServer().getPlayer(args[0]);
						if (target != null) {
							StringBuilder x = new StringBuilder();
							int x2;
							for (x2 = 1; x2 < args.length; x2++) {
								x.append(args[x2]).append(" ");
							}
							String from = ChatColor.AQUA + "(From "
									+ ((Player) s).getName() + ") "
									+ ChatColor.WHITE + " "
									+ x.toString().trim();
							String to = ChatColor.AQUA + "(To "
									+ target.getName() + ") " + ChatColor.WHITE
									+ " " + x.toString().trim();
							String op = ChatColor.AQUA + "("
									+ ((Player) s).getName() + " to "
									+ target.getName() + ") " + ChatColor.WHITE
									+ x.toString().trim();

							for (Player p : Bukkit.getServer().getOnlinePlayers()) {
								if (p.hasPermission("taurcmd.whisper.peek")) {
									p.sendMessage(op);
								}
							}
							target.sendMessage(from);
							((Player) s).sendMessage(to);
							System.out.println(op);
						} else {
							((Player) s)
									.sendMessage(TaurCmd.plugin.PlayerOffline);
						}
					} else {
						((Player) s)
								.sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					Player target = Bukkit.getServer().getPlayer(args[0]);
					if (target != null) {
						StringBuilder x = new StringBuilder();
						int x2;
						for (x2 = 1; x2 < args.length; x2++) {
							x.append(args[x2]).append(" ");
						}
						String from = ChatColor.AQUA + "(From Console) "
								+ ChatColor.WHITE + ": " + x.toString().trim();
						String to = ChatColor.AQUA + "(TO " + target.getName()
								+ ") " + ChatColor.WHITE + ": "
								+ x.toString().trim();
						String op = ChatColor.AQUA + "(Console to "
								+ target.getName() + ") " + ChatColor.WHITE + x.toString().trim();

						for (Player p : Bukkit.getServer().getOnlinePlayers()) {
							if (p.hasPermission("taurcmd.whisper.peek")) {
								p.sendMessage(op);
							}
						}
						target.sendMessage(from);
						((Player) s).sendMessage(to);
						System.out.println(op);
					}
				}
			}
		}
		return false;
	}
}