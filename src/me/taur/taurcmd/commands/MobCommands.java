package me.taur.taurcmd.commands;

import me.taur.taurcmd.TaurCmd;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.CreatureType;
import org.bukkit.entity.Player;

public class MobCommands implements CommandExecutor {
	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		if (s instanceof Player) {
			if (l.equalsIgnoreCase("spawnmob")) {
				if (args.length < 2) {
					((Player) s).sendMessage(ChatColor.YELLOW + "Usage: /" + l.toString() + " [mobname] [amount]");
					((Player) s).sendMessage(ChatColor.YELLOW + "Mobs: chicken, cow, pig, sheep, squid, enderman, pigman, wolf, cave_spider, creeper, ghast, silverfish, skeleton, slime, spider, zombie, ender_dragon, villager");
					}
				} else {
					if (s.hasPermission("taurcmd.spawnmob") || s.isOp()) {
						try {
							if (CreatureType.valueOf(args[0].toUpperCase()) != null) {
								int mob = Integer.parseInt(args[1]);
								Location block = ((Player) s).getTargetBlock(null, 0).getRelative(BlockFace.UP, 2).getLocation();
								for (int i = 0; i < mob; i++) {
									((Player) s).getWorld().spawnCreature(block,
											CreatureType.valueOf(args[0]
													.toUpperCase()));
								}
								((Player) s).sendMessage(ChatColor.GREEN + "Spawned " + args[1] + " " + args[0] + "(s)");
							}
						} catch (IllegalArgumentException e) {
							((Player) s).sendMessage(ChatColor.RED + "This '" + args[0] + "' you mention of does not exist.");
						}
					} else {
						((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
					}
				}
		} else {
			s.sendMessage(ChatColor.RED + "You can't spawn mobs in console!");
		}
		return false;
	}
}