package me.taur.taurcmd.commands;

import me.taur.taurcmd.TaurCmd;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TaurCommands implements CommandExecutor {

	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		if (l.equalsIgnoreCase("taurcmd")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.DARK_RED + "TaurCmd - " + ChatColor.YELLOW + " Version: " + TaurCmd.plugin.getDescription().getVersion());
					((Player) s).sendMessage(ChatColor.GOLD + " Developer: " + TaurCmd.plugin.getDescription().getAuthors());
					((Player) s).sendMessage(ChatColor.GOLD + " This plugin is designed to be used on Embarker servers only.");
					((Player) s).sendMessage(ChatColor.GRAY + " The ultimate server administration plugin out there.");
					((Player) s).sendMessage(ChatColor.YELLOW + "/" + l.toString() + " [reload/credits]");
				} else {
					((Player) s).sendMessage(ChatColor.DARK_RED + "TaurCmd - " + ChatColor.YELLOW + " Version: " + TaurCmd.plugin.getDescription().getVersion());
					((Player) s).sendMessage(ChatColor.GOLD + " Developer: " + TaurCmd.plugin.getDescription().getAuthors());
					((Player) s).sendMessage(ChatColor.GOLD + " This plugin is designed to be used on Embarker servers only.");
					((Player) s).sendMessage(ChatColor.GRAY + " The ultimate server administration plugin out there.");
					((Player) s).sendMessage(ChatColor.YELLOW + "/taurcmd [reload/credits]");
				}
			} else {
				if (args[0].equalsIgnoreCase("reload")) {
					if (s instanceof Player) {
						if (s.hasPermission("taurcmd.reload")) {
							TaurCmd.plugin.reloadConfig();
							((Player) s).sendMessage(ChatColor.GREEN + "TaurCmd's configuration successfully reloaded!");
						} else {
							((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
						}
					} else {
						TaurCmd.plugin.reloadConfig();
						s.sendMessage(ChatColor.GREEN + "TaurCmd's configuration has successfully reloaded!");
					}
				}

				if (args[0].equalsIgnoreCase("credits")) {
					if (s instanceof Player) {
						((Player) s).sendMessage(ChatColor.DARK_RED + "TaurCmd - " + ChatColor.YELLOW + " Version: " + TaurCmd.plugin.getDescription().getVersion());
						((Player) s).sendMessage(ChatColor.GOLD + " Developer: " + TaurCmd.plugin.getDescription().getAuthors());
						((Player) s).sendMessage(ChatColor.GOLD + " This plugin is designed to be used on Embarker servers only.");
						((Player) s).sendMessage(ChatColor.GRAY + " The ultimate server administration plugin out there.");
					} else {
						((Player) s).sendMessage(ChatColor.DARK_RED + "TaurCmd - " + ChatColor.YELLOW + " Version: " + TaurCmd.plugin.getDescription().getVersion());
						((Player) s).sendMessage(ChatColor.GOLD + " Developer: " + TaurCmd.plugin.getDescription().getAuthors());
						((Player) s).sendMessage(ChatColor.GOLD + " This plugin is designed to be used on Embarker servers only.");
						((Player) s).sendMessage(ChatColor.GRAY + " The ultimate server administration plugin out there.");
					}
				}
			}
		}
		
		if (l.equalsIgnoreCase("dummy")) {
			if (s instanceof Player) {
				if (s.hasPermission("taurcmd.dummy")) {
					((Player) s).sendMessage(ChatColor.GREEN + "This is a dummy reply!");
				} else {
					((Player) s).sendMessage(ChatColor.RED + "You weren't allowed to get a dummy reply, but here's one anyway.");
				}
			}
		}
		
		
		if (l.equalsIgnoreCase("memory")) {
			if (s instanceof Player) {
				if (s.hasPermission("taurcmd.memory")) {
					Runtime rt = Runtime.getRuntime();
					double max = Math.floor(rt.maxMemory() / 1024.0 / 1024.0);
					double free = Math.floor(rt.freeMemory() / 1024.0 / 1024.0);
					((Player) s).sendMessage(ChatColor.GREEN + "" + free + " / " + max + " memory available.");
				} else {
					((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
				}
			} else {
				Runtime rt = Runtime.getRuntime();
				double max = Math.floor(rt.maxMemory() / 1024.0 / 1024.0);
				double free = Math.floor(rt.freeMemory() / 1024.0 / 1024.0);
				s.sendMessage(ChatColor.GREEN + "" + free + " / " + max + " memory available.");
			}
		}
		return false;
	}

}
