package me.taur.taurcmd.commands;

import me.taur.taurcmd.TaurCmd;

import org.bukkit.command.CommandExecutor;

public class CommandRegistration {
	public static void SetupCommands() {

		CommandExecutor TeleporterCommands = new TeleportCommands();
		TaurCmd.plugin.getCommand("tp").setExecutor(TeleporterCommands);
		TaurCmd.plugin.getCommand("tphere").setExecutor(TeleporterCommands);
		TaurCmd.plugin.getCommand("tpall").setExecutor(TeleporterCommands);
		TaurCmd.plugin.getCommand("put").setExecutor(TeleporterCommands);
		TaurCmd.plugin.getCommand("tpworld").setExecutor(TeleporterCommands);
		TaurCmd.plugin.getCommand("spawn").setExecutor(TeleporterCommands);
		TaurCmd.plugin.getCommand("setspawn").setExecutor(TeleporterCommands);

		CommandExecutor Taur = new TaurCommands();
		TaurCmd.plugin.getCommand("taurcmd").setExecutor(Taur);
		TaurCmd.plugin.getCommand("dummy").setExecutor(Taur);
		TaurCmd.plugin.getCommand("memory").setExecutor(Taur);

		CommandExecutor JoinLeave = new JoinLeaveCommands();
		TaurCmd.plugin.getCommand("join").setExecutor(JoinLeave);
		TaurCmd.plugin.getCommand("leave").setExecutor(JoinLeave);

		CommandExecutor GameMode = new GameModeCommands();
		TaurCmd.plugin.getCommand("creative").setExecutor(GameMode);
		TaurCmd.plugin.getCommand("survival").setExecutor(GameMode);

		CommandExecutor Sky = new SkyCommands();
		TaurCmd.plugin.getCommand("time").setExecutor(Sky);
		TaurCmd.plugin.getCommand("weather").setExecutor(Sky);

		CommandExecutor PlayerCommands = new PlayerCommands();
		TaurCmd.plugin.getCommand("heal").setExecutor(PlayerCommands);
		TaurCmd.plugin.getCommand("god").setExecutor(PlayerCommands);
		TaurCmd.plugin.getCommand("slap").setExecutor(PlayerCommands);
		TaurCmd.plugin.getCommand("w").setExecutor(PlayerCommands);
		TaurCmd.plugin.getCommand("clear").setExecutor(PlayerCommands);
		TaurCmd.plugin.getCommand("kill").setExecutor(PlayerCommands);
		TaurCmd.plugin.getCommand("i").setExecutor(PlayerCommands);
		TaurCmd.plugin.getCommand("more").setExecutor(PlayerCommands);

		CommandExecutor ModCommands = new ModerationCommands();
		TaurCmd.plugin.getCommand("freeze").setExecutor(ModCommands);
		TaurCmd.plugin.getCommand("unfreeze").setExecutor(ModCommands);
		TaurCmd.plugin.getCommand("nocmd").setExecutor(ModCommands);
		TaurCmd.plugin.getCommand("usecmd").setExecutor(ModCommands);
		TaurCmd.plugin.getCommand("mute").setExecutor(ModCommands);
		TaurCmd.plugin.getCommand("unmute").setExecutor(ModCommands);
		TaurCmd.plugin.getCommand("who").setExecutor(ModCommands);

		CommandExecutor MobCommands = new MobCommands();
		TaurCmd.plugin.getCommand("spawnmob").setExecutor(MobCommands);

		CommandExecutor help = new HelpCommands();
		TaurCmd.plugin.getCommand("help").setExecutor(help);
		
		CommandExecutor Say = new SayCommand();
		TaurCmd.plugin.getCommand("say").setExecutor(Say);
		TaurCmd.plugin.getCommand("w").setExecutor(Say);
		
	}
}
