package me.taur.taurcmd.commands;

import me.taur.taurcmd.TaurCmd;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ModerationCommands implements CommandExecutor {
	public boolean onCommand(CommandSender s, Command c, String l, String[] args) {
		if (l.equalsIgnoreCase("freeze")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					((Player) s).sendMessage("Usage: /" + l.toString() + " [player]");
				} else {
					s.sendMessage(ChatColor.YELLOW + "Usage: /freeze [player]");
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.freeze")) {
						Player target = Bukkit.getServer().getPlayer(args[0]);
						if (target != null) {
							TaurCmd.plugin.getConfig().set("database.players." + ((Player) s).getName() + ".frozen", true);
							((Player) s).sendMessage(ChatColor.DARK_GREEN + target.getName() + " has been frozen!");
							target.sendMessage(ChatColor.DARK_GREEN + "You have been frozen by " + ((Player) s).getName());
							TaurCmd.plugin.saveConfig();
						} else {
							((Player) s).sendMessage(TaurCmd.plugin.PlayerOffline);
						}
					} else {
						((Player) s).sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					Player target = Bukkit.getServer().getPlayer(args[0]);
					if (target != null) {
						TaurCmd.plugin.getConfig().set("database.players." + ((Player) s).getName() + ".frozen", true);
						s.sendMessage(ChatColor.GREEN + "You froze " + target.getName());
						TaurCmd.plugin.saveConfig();
					} else {
						s.sendMessage(TaurCmd.plugin.PlayerOffline);
					}
				}
			}
		}

		if (l.equalsIgnoreCase("unfreeze")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					((Player) s).sendMessage("Usage: /" + l.toString() + " [player]");
				} else {
					s.sendMessage(ChatColor.YELLOW + "Usage: /unfreeze [player]");
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.freeze")) {
						Player target = Bukkit.getServer().getPlayer(args[0]);
						if (target != null) {
							TaurCmd.plugin.getConfig().set("database.players." + ((Player) s).getName() + ".frozen", false);
							((Player) s).sendMessage(ChatColor.GREEN
									+ "You have unfrozen " + target.getName());
							target.sendMessage(ChatColor.GREEN
									+ ((Player) s).getName()
									+ " has unfrozen you.");
							TaurCmd.plugin.saveConfig();
						} else {
							((Player) s)
									.sendMessage(TaurCmd.plugin.PlayerOffline);
						}
					} else {
						((Player) s)
								.sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					Player target = Bukkit.getServer().getPlayer(args[0]);
					if (target != null) {
						TaurCmd.plugin.getConfig().set("database.players." + ((Player) s).getName() + ".frozen", false);
						TaurCmd.plugin.saveConfig();
						s.sendMessage(ChatColor.GREEN + "You have unfrozen "
								+ target.getName());
					} else {
						s.sendMessage(TaurCmd.plugin.PlayerOffline);
					}
				}
			}
		}

		if (l.equalsIgnoreCase("nocmd")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.YELLOW + "Usage: /" + l.toString() + " [player]");
				} else {
					s.sendMessage(ChatColor.YELLOW + "Usage: /nocmd [player]");
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.nocmd")) {
						Player target = Bukkit.getServer().getPlayer(args[0]);
						if (target != null) {
							TaurCmd.plugin.getConfig().set("database.players." + ((Player) s).getName() + ".nocmd", true);
							TaurCmd.plugin.saveConfig();
							((Player) s).sendMessage(ChatColor.DARK_GREEN
									+ target.getName()
									+ " has been stripped of commands!");
							target.sendMessage(ChatColor.DARK_GREEN
									+ ((Player) s).getName()
									+ " stripped you of commands!");
						} else {
							((Player) s)
									.sendMessage(TaurCmd.plugin.PlayerOffline);
						}
					} else {
						((Player) s)
								.sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					Player target = Bukkit.getServer().getPlayer(args[0]);
					if (target != null) {
						TaurCmd.plugin.getConfig().set("database.players." + ((Player) s).getName() + ".nocmd", true);
						TaurCmd.plugin.saveConfig();
						s.sendMessage(ChatColor.GREEN + "You have disabled "
								+ target.getName() + "'s usage of commands!");
					} else {
						s.sendMessage(TaurCmd.plugin.PlayerOffline);
					}
				}
			}
		}

		if (l.equalsIgnoreCase("usecmd")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.YELLOW + "Usage: /" + l.toString() + " [player]");
				} else {
					s.sendMessage(ChatColor.YELLOW + "Usage: /usecmd [player]");
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.nocmd")) {
						Player target = Bukkit.getServer().getPlayer(args[0]);
						if (target != null) {
							TaurCmd.plugin.getConfig().set("database.players." + ((Player) s).getName() + ".nocmd", false);
							TaurCmd.plugin.saveConfig();
							((Player) s).sendMessage(ChatColor.GREEN
									+ target.getName()
									+ " has been granted use of commands!");
							target.sendMessage(ChatColor.GREEN
									+ ((Player) s).getName()
									+ " granted you commands!");
						} else {
							((Player) s)
									.sendMessage(TaurCmd.plugin.PlayerOffline);
						}
					} else {
						((Player) s)
								.sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					Player target = Bukkit.getServer().getPlayer(args[0]);
					if (target != null) {
						TaurCmd.plugin.getConfig().set("database.players." + ((Player) s).getName() + ".nocmd", false);
						TaurCmd.plugin.saveConfig();
						s.sendMessage(TaurCmd.plugin.PlayerOffline);
					}
				}
			}
		}

		if (l.equalsIgnoreCase("mute")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.YELLOW + "Usage: /" + l.toString() + " [player]");
				} else {
					s.sendMessage(ChatColor.YELLOW + "Usage: /mute [player]");
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.mute")) {
						Player target = Bukkit.getServer().getPlayer(args[0]);
						if (target != null) {
							TaurCmd.plugin.getConfig().set("database.players." + ((Player) s).getName() + ".muted", true);
							((Player) s).sendMessage(ChatColor.GREEN
									+ "You have muted " + target.getName());
							target.sendMessage(ChatColor.DARK_GREEN
									+ ((Player) s).getName()
									+ " has muted you!");
							TaurCmd.plugin.saveConfig();
						} else {
							((Player) s)
									.sendMessage(TaurCmd.plugin.PlayerOffline);
						}
					} else {
						((Player) s)
								.sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					Player target = Bukkit.getServer().getPlayer(args[0]);
					if (target != null) {
						TaurCmd.plugin.getConfig().set("database.players." + ((Player) s).getName() + ".muted", true);
						s.sendMessage(ChatColor.GREEN + "You muted "
								+ target.getName() + "!");
						TaurCmd.plugin.saveConfig();
					} else {
						s.sendMessage(TaurCmd.plugin.PlayerOffline);
					}
				}
			}
		}

		if (l.equalsIgnoreCase("unmute")) {

			if (args.length < 1) {
				if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.YELLOW + "Usage: /" + l.toString() + " [player]");
				} else {
					s.sendMessage(ChatColor.YELLOW + "Usage: /unmute [player]");
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.mute")) {
						Player target = Bukkit.getServer().getPlayer(args[0]);
						if (target != null) {
							TaurCmd.plugin.getConfig().set("database.players." + ((Player) s).getName() + ".muted", false);
							TaurCmd.plugin.saveConfig();
							((Player) s).sendMessage(ChatColor.GREEN
									+ "You have unmuted " + target.getName());
							target.sendMessage(ChatColor.GREEN
									+ ((Player) s).getName()
									+ " has unmuted you!");
						} else {
							((Player) s)
									.sendMessage(TaurCmd.plugin.PlayerOffline);
						}
					} else {
						((Player) s)
								.sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					Player target = Bukkit.getServer().getPlayer(args[0]);
					if (target != null) {
						TaurCmd.plugin.getConfig().set("database.players." + ((Player) s).getName() + ".muted", false);
						TaurCmd.plugin.saveConfig();
						s.sendMessage(ChatColor.GREEN + "You unmuted "
								+ target.getName());
					} else {
						s.sendMessage(TaurCmd.plugin.PlayerOffline);
					}
				}
			}
		}

		if (l.equalsIgnoreCase("who")) {
			if (args.length < 1) {
				if (s instanceof Player) {
					((Player) s).sendMessage(ChatColor.YELLOW + "Usage: /" + l.toString() + " [player]");
				} else {
					s.sendMessage(ChatColor.YELLOW + "Usage: /who [player]");
				}
			} else {
				if (s instanceof Player) {
					if (s.hasPermission("taurcmd.who")) {
						Player target = Bukkit.getServer().getPlayer(args[0]);
						if (target != null) {
							((Player) s).sendMessage(ChatColor.DARK_GREEN
									+ "Information on: "
									+ target.getDisplayName());
							((Player) s).sendMessage(ChatColor.GREEN
									+ "Real Name: " + ChatColor.WHITE
									+ target.getName());
							((Player) s).sendMessage(ChatColor.GREEN
									+ "Current World: " + ChatColor.WHITE
									+ target.getWorld().getName());
							((Player) s).sendMessage(ChatColor.GREEN
									+ "IP Address: " + ChatColor.WHITE
									+ target.getAddress());
							((Player) s).sendMessage(ChatColor.GREEN
									+ "Hostname: " + ChatColor.WHITE
									+ target.getAddress().getHostName());
							((Player) s).sendMessage(ChatColor.GREEN
									+ "Co-Ordinates: " + ChatColor.WHITE
									+ target.getLocation().getX() + ", "
									+ target.getLocation().getY() + ", "
									+ target.getLocation().getZ());
						} else {
							((Player) s)
									.sendMessage(TaurCmd.plugin.PlayerOffline);
						}
					} else {
						((Player) s)
								.sendMessage(TaurCmd.plugin.NoPermission);
					}
				} else {
					Player target = Bukkit.getServer().getPlayer(args[0]);
					if (target != null) {
						s.sendMessage(ChatColor.DARK_GREEN + "Information on: "
								+ target.getDisplayName());
						s.sendMessage(ChatColor.GREEN + "Real Name: "
								+ ChatColor.WHITE + target.getName());
						s.sendMessage(ChatColor.GREEN + "Current World: "
								+ ChatColor.WHITE + target.getWorld().getName());
						s.sendMessage(ChatColor.GREEN + "IP Address: "
								+ ChatColor.WHITE + target.getAddress());
						s.sendMessage(ChatColor.GREEN + "Hostname: "
								+ ChatColor.WHITE
								+ target.getAddress().getHostName());
						s.sendMessage(ChatColor.GREEN + "Co-Ordinates: "
								+ ChatColor.WHITE + target.getLocation().getX()
								+ ", " + target.getLocation().getY() + ", "
								+ target.getLocation().getZ());
					} else {
						s.sendMessage(TaurCmd.plugin.PlayerOffline);
					}
				}
			}
		}
		return false;
	}
}