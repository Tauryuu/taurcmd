package me.taur.taurcmd.config;

import me.taur.taurcmd.TaurCmd;

public class ConfigSetup {

	public static void start() {

		if (TaurCmd.plugin.getConfig().get("settings.mob.allow-enderman-griefing") == null) {
			TaurCmd.plugin.getConfig().set("settings.mob.allow-enderman-griefing", false);
			TaurCmd.plugin.saveConfig();
		}
		
		if (TaurCmd.plugin.getConfig().get("settings.mob.allow-creeper-explosions") == null) {
			TaurCmd.plugin.getConfig().set("settings.mob.allow-creeper-explosions", true);
			TaurCmd.plugin.saveConfig();
		}
		
		if (TaurCmd.plugin.getConfig().get("settings.blocks.allow-bedrock") == null) {
			TaurCmd.plugin.getConfig().set("settings.blocks.allow-bedrock", false);
			TaurCmd.plugin.saveConfig();
		}

		if (TaurCmd.plugin.getConfig().get("settings.blocks.allow-lava") == null) {
			TaurCmd.plugin.getConfig().set("settings.blocks.allow-lava", true);
			TaurCmd.plugin.saveConfig();
		}
		
		if (TaurCmd.plugin.getConfig().get("settings.blocks.allow-tnt") == null) {
			TaurCmd.plugin.getConfig().set("settings.blocks.allow-tnt", true);
			TaurCmd.plugin.saveConfig();
		}

		if (TaurCmd.plugin.getConfig().get("settings.ore.broadcast-coalore") == null) {
			TaurCmd.plugin.getConfig().set("settings.ore.broadcast-coalore", false);
			TaurCmd.plugin.saveConfig();
		}

		if (TaurCmd.plugin.getConfig().get("settings.ore.broadcast-ironore") == null) {
			TaurCmd.plugin.getConfig().set("settings.ore.broadcast-ironore", false);
			TaurCmd.plugin.saveConfig();
		}

		if (TaurCmd.plugin.getConfig().get("settings.ore.broadcast-goldore") == null) {
			TaurCmd.plugin.getConfig().set("settings.ore.broadcast-goldore", false);
			TaurCmd.plugin.saveConfig();
		}

		if (TaurCmd.plugin.getConfig().get("settings.ore.broadcast-diamondore") == null) {
			TaurCmd.plugin.getConfig().set("settings.ore.broadcast-diamondore", false);
			TaurCmd.plugin.saveConfig();
		}

		if (TaurCmd.plugin.getConfig().get("settings.ore.broadcast-redstoneore") == null) {
			TaurCmd.plugin.getConfig().set("settings.ore.broadcast-redstoneore", false);
			TaurCmd.plugin.saveConfig();
		}

		if (TaurCmd.plugin.getConfig().get("settings.ore.broadcast-lapislazuliore") == null) {
			TaurCmd.plugin.getConfig().set("settings.ore.broadcast-lapislazuliore", false);
			TaurCmd.plugin.saveConfig();
		}

		if (TaurCmd.plugin.getConfig().getString("settings.motd.message") == null) {
			TaurCmd.plugin.getConfig().set("settings.motd.message", "Please visit http://embarker.me!");
			TaurCmd.plugin.saveConfig();
		}
		
		if (TaurCmd.plugin.getConfig().getString("database") != "Do not edit this section!") {
			TaurCmd.plugin.getConfig().set("database", "Do not edit this section!");
			TaurCmd.plugin.saveConfig();
		}

	}

}
