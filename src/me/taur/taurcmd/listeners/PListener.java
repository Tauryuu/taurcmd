package me.taur.taurcmd.listeners;

import me.taur.taurcmd.TaurCmd;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerListener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PListener extends PlayerListener {

	static TaurCmd plugin;

	@Override
	public void onPlayerMove(PlayerMoveEvent e) {
		if (TaurCmd.plugin.getConfig().getBoolean("database.players." + e.getPlayer().getName() + ".frozen")) {
			e.setCancelled(true);
		}
	}
	
	@Override
	public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent e) {
		if (TaurCmd.plugin.getConfig().getBoolean("database.players." + e.getPlayer().getName() + ".nocmd")) {
			e.getPlayer().sendMessage(ChatColor.RED + "Give me a good reason why I should process your command.");
			e.setCancelled(true);
		}
	}

	@Override
	public void onPlayerChat(PlayerChatEvent e) {
		if (TaurCmd.plugin.getConfig().getBoolean("database.players." + e.getPlayer().getName() + ".muted")) {
			e.getPlayer().sendMessage(ChatColor.RED + "Shh. Don't talk.");
			e.setCancelled(true);
		}

		/*
		 * if(CommandBin.plugin.getConfig().getBoolean("settings.customchat")) {
		 * String message = e.getMessage(); String lol =
		 * message.replaceAll("(&([a-f0-9]))", "§$2"); e.setCancelled(true);
		 * String test =
		 * CommandBin.permissionHandler.getUserPrefix(e.getPlayer()
		 * .getWorld().getName(), e.getPlayer().getName());
		 * Bukkit.getServer().broadcastMessage("[" + test + "] " +
		 * e.getPlayer().getName() + ": " + lol); }
		 */
		return;
	}
		
	@Override
	public void onPlayerLogin(PlayerLoginEvent e) {			
		
		String FLY_DISABLER = "&f &f &1 &0 &2 &4 &3 &9 &2 &0 &0 &1 ";
		String CHEAT_DISABLER = "&f &f &2 &0 &4 &8 &3 &9 &2 &0 &0 &2 &3 &9 &2 &0 &0 &3 ";
		String REI_ALL_ENABLER = "&0 &0 &1 &2 &3 &4 &5 &6 &7 &e &f ";
		String REI_CAVE_ENABLER = "&0 &0 &1 &e &f ";
		String REI_ENTITY_ENABLER = "&0 &0 &1 &2 &3 &5 &e &f ";
		
		String FLY_MSG = FLY_DISABLER.replaceAll("(&([a-f0-9]))", "\u00A7$2");
		String CHEAT_MSG = CHEAT_DISABLER.replaceAll("(&([a-f0-9]))", "\u00A7$2");
		String REI_ALL_MSG = REI_ALL_ENABLER.replaceAll("(&([a-f0-9]))", "\u00A7$2");
		String REI_CAVE_MSG = REI_CAVE_ENABLER.replaceAll("(&([a-f0-9]))", "\u00A7$2");
		String REI_ENTITY_MSG = REI_ENTITY_ENABLER.replaceAll("(&([a-f0-9]))", "\u00A7$2");
		
		Player p = e.getPlayer();
				
			if(!p.hasPermission("taurcmd.flymod")) {
				p.sendMessage(FLY_MSG);
			}
			if(!p.hasPermission("taurcmd.cheatmod")) {
				p.sendMessage(CHEAT_MSG);
			}
			if(!p.hasPermission("taurcmd.reiall")) {
				p.sendMessage(REI_ALL_MSG);
			}
			if(!p.hasPermission("taurcmd.reicave")) {
				p.sendMessage(REI_CAVE_MSG);
			}
			if(!p.hasPermission("taurcmd.reientity")) {
				p.sendMessage(REI_ENTITY_MSG);
			}
			
		return;
	}

	@Override
	public void onPlayerJoin(PlayerJoinEvent e) {
		String MOTD = TaurCmd.plugin.getConfig().getString("settings.motd.message");
	    for (ChatColor color : ChatColor.values()) {
	        String MOTD_COLOR = MOTD.replace(String.format("&%x", color.getCode()), color.toString());
	        
	        for (String MOTD_BREAK : MOTD_COLOR.split("/break")) {
	            e.getPlayer().sendMessage(MOTD_BREAK.replace("[p]", e.getPlayer().getName()));
	        }
	    }
		return;
	}

	@Override
	public void onPlayerRespawn(PlayerRespawnEvent e) {
		e.getPlayer().teleport(e.getPlayer().getWorld().getSpawnLocation());
		return;
	}
}
