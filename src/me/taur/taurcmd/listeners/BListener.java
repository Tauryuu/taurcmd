package me.taur.taurcmd.listeners;

import me.taur.taurcmd.TaurCmd;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockListener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BListener extends BlockListener {

	public void onBlockPlace(BlockPlaceEvent e) {

		if (TaurCmd.plugin.getConfig().get("block.allow-tnt") != null) {
			if (TaurCmd.plugin.getConfig().get("block.allow-tnt").equals(false)) {
				if (e.getBlock().getType() == Material.TNT) {
					if (!e.getPlayer().hasPermission("taurcmd.tntbypass")) {
						e.setCancelled(true);
						e.getPlayer().sendMessage(ChatColor.RED + "TNT cannot be placed.");
					}
				}
			}
		}

		if (TaurCmd.plugin.getConfig().get("blocks.allow-bedrock") != null) {
			if (TaurCmd.plugin.getConfig().get("blocks.allow-bedrock").equals(false)) {
				if (e.getBlock().getType() == Material.BEDROCK) {
					if (!e.getPlayer().hasPermission("taurcmd.bedrockbypass")) {
						e.setCancelled(true);
						e.getPlayer().sendMessage(ChatColor.RED + "Bedrock cannot be placed.");
					}
				}
			}
		}

		if (TaurCmd.plugin.getConfig().get("blocks.allow-lava") != null) {
			if (TaurCmd.plugin.getConfig().get("blocks.allow-lava").equals(false)) {
				if (e.getBlock().getType() == Material.LAVA || e.getBlock().getType() == Material.LAVA_BUCKET) {
					if (!e.getPlayer().hasPermission("taurcmd.lavabypass")) {
						e.setCancelled(true);
						e.getPlayer().sendMessage(ChatColor.RED + "Lava cannot be placed.");
					}
				}
			}
		}

		return;
	}

	public void onBlockBreak(BlockBreakEvent e) {

		if (TaurCmd.plugin.getConfig().getBoolean("ore.broadcast-coalore")) {
			if (e.getBlock().getType() == Material.COAL_ORE) {
				Bukkit.getServer().broadcastMessage(ChatColor.AQUA + e.getPlayer().getName()  + " mined some Coal Ore!");
			}
		}

		if (TaurCmd.plugin.getConfig().getBoolean("ore.broadcast-ironore")) {
			if (e.getBlock().getType() == Material.IRON_ORE) {
				Bukkit.getServer().broadcastMessage(ChatColor.AQUA + e.getPlayer().getName() + " mined some Iron Ore!");
			}
		}

		if (TaurCmd.plugin.getConfig().getBoolean("ore.broadcast-goldore")) {
			if (e.getBlock().getType() == Material.GOLD_ORE) {
				Bukkit.getServer().broadcastMessage(ChatColor.AQUA + e.getPlayer().getName() + " mined some Gold Ore!");
			}
		}

		if (TaurCmd.plugin.getConfig().getBoolean("ore.broadcast-diamondore")) {
			if (e.getBlock().getType() == Material.DIAMOND_ORE) {
				Bukkit.getServer().broadcastMessage(ChatColor.AQUA + e.getPlayer().getName() + " mined some Diamond Ore!");
			}
		}

		if (TaurCmd.plugin.getConfig().getBoolean("ore.broadcast-redstoneore")) {
			if (e.getBlock().getType() == Material.REDSTONE_ORE) {
				Bukkit.getServer().broadcastMessage(ChatColor.AQUA + e.getPlayer().getName() + " mined some Redstone Ore!");
			}
		}

		if (TaurCmd.plugin.getConfig().getBoolean("ore.broadcast-lapislazuliore")) {
			if (e.getBlock().getType() == Material.LAPIS_ORE) {
				Bukkit.getServer().broadcastMessage(ChatColor.AQUA + e.getPlayer().getName() + " mined some Lapis Lazuli Ore!");
			}
		}
		return;
	}

}
