package me.taur.taurcmd.listeners;

import me.taur.taurcmd.TaurCmd;

import org.bukkit.entity.Player;
import org.bukkit.event.entity.EndermanPickupEvent;
import org.bukkit.event.entity.EndermanPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityListener;

public class EListener extends EntityListener {

	@Override
	public void onEntityDamage(EntityDamageEvent e) {

		if (e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			if (TaurCmd.plugin.getConfig().getBoolean("database.players." + p.getName() + ".god")) {
				e.setCancelled(true);
			}
		}
		return;
	}

/*	@Override
	public void onEntityDeath(EntityDeathEvent e) {
		if (TaurCmd.plugin.getConfig().getBoolean("settings.dropxpondeath",false)) {
			e.setDroppedExp(0);
		}
		return;
	}
*/
	
	@Override
	public void onEntityExplode(EntityExplodeEvent e) {
		if (TaurCmd.plugin.getConfig().getBoolean("settings.mob.allow-creeper-explosions", false)) {
			e.setCancelled(true);
		}
		return;
	}

	@Override
	public void onEndermanPickup(EndermanPickupEvent e) {
		if (TaurCmd.plugin.getConfig().getBoolean("settings.mob.allow-enderman-griefing", false)) {
			e.setCancelled(true);
		}
		return;
	}

	@Override
	public void onEndermanPlace(EndermanPlaceEvent e) {
		if (TaurCmd.plugin.getConfig().getBoolean("settings.mob.allow-enderman-griefing", false)) {
			e.setCancelled(true);
		}
		return;
	}

}
