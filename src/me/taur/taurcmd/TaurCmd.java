package me.taur.taurcmd;

import me.taur.taurcmd.commands.CommandRegistration;
import me.taur.taurcmd.config.ConfigSetup;
import me.taur.taurcmd.listeners.EventRegistration;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public class TaurCmd extends JavaPlugin {

	public String Plugin = "[TaurCmd] ";
	public static TaurCmd plugin;
	public String NoPermission = ChatColor.WHITE + "Unknown command. Don't ask for help.";
	public String PlayerOffline = ChatColor.RED + "This player is offline";

	public void onEnable() {
		plugin = this;
		setupCmd();
		System.out.println(Plugin + "Enabled successfully.");
		System.out.println(Plugin + "You are running version " + this.getDescription().getVersion());
	}

	public void setupCmd() {
		EventRegistration.setup();
		ConfigSetup.start();
		CommandRegistration.SetupCommands();
	}

	public void onDisable() {
		System.out.println(Plugin + "Disabled successfully.");
	}
}